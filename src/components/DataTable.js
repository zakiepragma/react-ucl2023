import React, { useContext, useEffect, useState } from "react";
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  IconButton,
  Button,
  Input,
} from "@chakra-ui/react";
import { EditIcon, DeleteIcon } from "@chakra-ui/icons";
import { CrudContext } from "../contexts/CrudContext";

const DataTable = () => {
  const { clubs, deleteClub, updateClub } = useContext(CrudContext);
  const [editing, setEditing] = useState(false);
  const [name, setName] = useState("");
  const [id, setId] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    updateClub(id, name);
    setEditing(false);
    setName("");
    setId("");
  };

  const handleEdit = (club) => {
    setEditing(true);
    setName(club.name);
    setId(club.id);
  };

  return (
    <Table variant="simple" size="sm">
      <Thead>
        <Tr>
          <Th>No.</Th>
          <Th>Nama Klub</Th>
          <Th>Actions</Th>
        </Tr>
      </Thead>
      <Tbody>
        {clubs.map((club, index) => (
          <Tr key={index}>
            <Td>{index + 1}</Td>
            <Td>
              {editing && id === club.id ? (
                <Input
                  value={name}
                  onChange={(event) => setName(event.target.value)}
                />
              ) : (
                club.name
              )}
            </Td>
            <Td>
              {editing && id === club.id ? (
                <>
                  <Button
                    onClick={handleSubmit}
                    mr={2}
                    type="submit"
                    colorScheme="teal"
                    size="sm"
                  >
                    Save
                  </Button>
                  <Button
                    onClick={() => setEditing(false)}
                    colorScheme="orange"
                    size="sm"
                  >
                    Cancel
                  </Button>
                </>
              ) : (
                <>
                  <IconButton
                    aria-label="Edit"
                    icon={<EditIcon />}
                    mr={2}
                    colorScheme="green"
                    size="sm"
                    onClick={() => handleEdit(club)}
                  />
                  <IconButton
                    aria-label="Delete"
                    colorScheme="red"
                    icon={<DeleteIcon />}
                    size="sm"
                    onClick={() => deleteClub(club.id)}
                  />
                </>
              )}
            </Td>
          </Tr>
        ))}
      </Tbody>
    </Table>
  );
};

export default DataTable;
