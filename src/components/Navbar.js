import React from "react";
import { Box, Flex, Spacer, Text, useDisclosure } from "@chakra-ui/react";
import { HamburgerIcon } from "@chakra-ui/icons";

function Navbar() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Box bg="blue.500" w="100%" p={4} color="white">
        <Flex>
          <Box p="2">
            <Text fontSize="xl" fontWeight="bold">
              1/4 FINAL UCL 2023
            </Text>
          </Box>
          <Spacer />
          <Box display={{ base: "none", md: "block" }}>
            <Flex>
              <Box p="2">
                <Text fontSize="xl">
                  <a href="https://gitlab.com/zakiepragma">Gitlab</a>
                </Text>
              </Box>
              <Box p="2">
                <Text fontSize="xl">
                  <a href="https://github.com/programmercintasunnah">Github</a>
                </Text>
              </Box>
              <Box p="2">
                <Text fontSize="xl">
                  <a href="https://www.youtube.com/watch?v=vBtPm0SnvW8&list=PLGwA21JLpwoMtXpnQHnhv0kasbL2tXWlt&index=38">
                    Youtube
                  </a>
                </Text>
              </Box>
            </Flex>
          </Box>
          <Box display={{ base: "block", md: "none" }} onClick={onOpen}>
            <HamburgerIcon />
          </Box>
        </Flex>
      </Box>
      <Box display={{ base: isOpen ? "block" : "none", md: "none" }} pb={4}>
        <Box p="2">
          <Text fontSize="xl">Home</Text>
        </Box>
        <Box p="2">
          <Text fontSize="xl">About</Text>
        </Box>
        <Box p="2">
          <Text fontSize="xl">Contact</Text>
        </Box>
      </Box>
    </>
  );
}

export default Navbar;
