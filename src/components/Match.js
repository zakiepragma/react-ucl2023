import { useContext, useState } from "react";
import {
  Box,
  Flex,
  FormControl,
  Select,
  Input,
  Button,
  Text,
  Heading,
} from "@chakra-ui/react";
import { CrudContext } from "../contexts/CrudContext";

const Match = () => {
  const { clubs } = useContext(CrudContext);

  const [matches, setMatches] = useState([
    {
      homeTeam: "",
      homeScore: "",
      awayScore: "",
      awayTeam: "",
    },
    {
      homeTeam: "",
      homeScore: "",
      awayScore: "",
      awayTeam: "",
    },
    {
      homeTeam: "",
      homeScore: "",
      awayScore: "",
      awayTeam: "",
    },
    {
      homeTeam: "",
      homeScore: "",
      awayScore: "",
      awayTeam: "",
    },
  ]);

  const handleMatchChange = (index, field, value) => {
    setMatches((prevMatches) => {
      const newMatches = [...prevMatches];
      newMatches[index][field] = value;
      return newMatches;
    });
  };

  const handleRandomSkor = () => {
    const newMatches = matches.map((match) => {
      const homeScore = Math.floor(Math.random() * 6);
      const awayScore = Math.floor(Math.random() * 6);
      return {
        ...match,
        homeScore,
        awayScore,
      };
    });
    setMatches(newMatches);
  };

  return (
    <Box mx="auto" textAlign="center">
      <Heading as="h1" size="lg" fontWeight="bold" mb="10">
        Quarter Final UCL 2023
      </Heading>
      {matches.map((match, index) => (
        <Flex
          key={index}
          direction="row"
          justifyContent="center"
          alignItems="center"
          mt="4"
        >
          <FormControl mr="4">
            <Select
              value={match.homeTeam}
              onChange={(e) =>
                handleMatchChange(index, "homeTeam", e.target.value)
              }
            >
              <option value="">-- Choose Home Team --</option>
              {clubs.map((club, index) => (
                <option key={index} value={club.name}>
                  {club.name}
                </option>
              ))}
            </Select>
          </FormControl>

          <FormControl mr="4">
            <Input
              type="number"
              max={99}
              value={match.homeScore}
              onChange={(e) =>
                handleMatchChange(index, "homeScore", e.target.value)
              }
            />
          </FormControl>
          <Text mr="4">VS</Text>
          <FormControl mr="4">
            <Input
              type="number"
              max={99}
              value={match.awayScore}
              onChange={(e) =>
                handleMatchChange(index, "awayScore", e.target.value)
              }
            />
          </FormControl>
          <FormControl mr="4">
            <Select
              value={match.awayTeam}
              onChange={(e) =>
                handleMatchChange(index, "awayTeam", e.target.value)
              }
            >
              <option value="">-- Choose Away Team --</option>
              {clubs.map((club, index) => (
                <option key={index} value={club.name}>
                  {club.name}
                </option>
              ))}
            </Select>
          </FormControl>
        </Flex>
      ))}
      <Flex
        direction="row"
        justifyContent="center"
        alignItems="center"
        mt="2"
        gap="2"
        p="4"
      >
        <Button colorScheme="teal" onClick={handleRandomSkor}>
          Skor Random
        </Button>
      </Flex>
    </Box>
  );
};

export default Match;
