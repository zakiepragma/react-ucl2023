import React, { useContext, useState } from "react";
import { Flex, Input, IconButton } from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import { CrudContext } from "../contexts/CrudContext";

const FormInput = () => {
  const { addClub } = useContext(CrudContext);
  const [newClubName, setNewClubName] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    addClub(newClubName);
    setNewClubName("");
  };

  return (
    <Flex as="form" onSubmit={handleSubmit} alignItems="center">
      <Input
        placeholder="Enter Club Name"
        value={newClubName}
        onChange={(event) => setNewClubName(event.target.value)}
        mr={2}
      />
      <IconButton
        type="submit"
        aria-label="Add"
        icon={<AddIcon />}
        colorScheme="blue"
      />
    </Flex>
  );
};

export default FormInput;
