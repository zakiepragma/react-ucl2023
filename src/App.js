import { Box, Grid } from "@chakra-ui/react";
import React from "react";
import DataTable from "./components/DataTable";
import FormInput from "./components/FormInput";
import Match from "./components/Match";
import Navbar from "./components/Navbar";
import CrudContextProvider from "./contexts/CrudContext";

const App = () => {
  return (
    <CrudContextProvider>
      <Navbar />
      <Box maxW="600px" m="auto" mt={4} p={4}>
        <FormInput />
      </Box>
      <Grid
        templateColumns={{ sm: "1fr", md: "repeat(2, 1fr)" }}
        gap={6}
        maxW="1200px"
        mx="auto"
        mt="8"
      >
        <DataTable />
        <Match />
      </Grid>
    </CrudContextProvider>
  );
};

export default App;
