import React, { createContext, useState, useEffect } from "react";
import axios from "axios";

export const CrudContext = createContext();

const CrudContextProvider = ({ children }) => {
  const [clubs, setClubs] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:3001/clubs")
      .then((response) => setClubs(response.data))
      .catch((error) => console.log(error));
  }, [clubs]);

  const addClub = (newClub) => {
    console.log(newClub);
    axios
      .post("http://localhost:3001/clubs", { name: newClub })
      .then((response) => setClubs([...clubs, response.data]))
      .catch((error) => console.log(error));
  };

  const updateClub = (id, updatedClub) => {
    axios
      .put(`http://localhost:3001/clubs/${id}`, { name: updatedClub })
      .then((response) =>
        setClubs(clubs.map((club) => (club.id === id ? response.data : club)))
      )
      .catch((error) => console.log(error));
  };

  const deleteClub = (id) => {
    axios
      .delete(`http://localhost:3001/clubs/${id}`)
      .then(() => setClubs(clubs.filter((club) => club.id !== id)))
      .catch((error) => console.log(error));
  };

  return (
    <CrudContext.Provider value={{ clubs, addClub, updateClub, deleteClub }}>
      {children}
    </CrudContext.Provider>
  );
};

export default CrudContextProvider;
